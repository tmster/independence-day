require 'rails_helper'

RSpec.describe Identity, type: :model do
  it { is_expected.to have_field(:provider).of_type(String) }

  it { is_expected.to belong_to(:user) }

  it { is_expected.to validate_presence_of(:user_id) }
  it { is_expected.to validate_uniqueness_of(:user_id).scoped_to(:provider) }

  describe '.find_for_oauth' do
    pending
  end
end
