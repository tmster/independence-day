# Controller for getting callbacks from devise
class OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def facebook
    result = SignInExternalUser.call(auth: env["omniauth.auth"], user: current_user)

    if result.success?
      sign_in_and_redirect result.user, event: :authentication
      SaveToken.call(auth: env["omniauth.auth"], user: result.user)
      set_flash_message(:notice, :success, kind: "facebook".capitalize) if is_navigational_format?
    else
      session["devise.facebook_data"] = env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

  def after_sign_in_path_for(resource)
    if resource.email_verified?
      super resource
    else
      finish_signup_user_path(resource)
    end
  end
end
