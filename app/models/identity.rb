class Identity
  include Mongoid::Document

  field :provider, type: String

  belongs_to :user

  validates_presence_of :user_id, :provider
  validates_uniqueness_of :user_id, :scope => :provider

  def self.find_for_oauth(auth)
    find_or_create_by(user_id: auth.user_id, provider: auth.provider)
  end
end
