class SaveToken
  include Interactor

  def call
    params = generate_params(context)

    response = RestClient.post "#{System::Settings.apis.token.url}tokens", token: params

    context.fail!(message: "token.failure") if response.code != 200

    response
  end

  private

  def generate_params(context)
    {
      uid: context.auth.uid,
      email: context.user.email,
      token: context.auth.credentials.token,
      expires_at: context.auth.credentials.expired_at
    }
  end
end
