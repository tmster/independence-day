class SignInExternalUser
  include Interactor

  def call
    # Get the identity and user if they exist
    identity = Identity.find_for_oauth(context[:auth])

    user = context[:user] ? signed_in_resource : identity.user
    auth = context[:auth]
    # Create the user if needed

    if user.nil?

      email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
      email = auth.info.email if email_is_verified
      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if user.nil?
        user = User.new(
          name: auth.extra.raw_info.name,
          email: email ? email : "#{User::TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
          password: Devise.friendly_token[0,20]
        )
        user.skip_confirmation!

        context.fail!(message: "sign_in.failure") unless user.save
      end
    end
    # Associate the identity with the user if needed
    if identity.user != user
      identity.user = user
      context.fail!(message: "sign_in.failure") unless user.save
    end

    context.fail!(message: "sign_in.failure") unless user.persisted?
    context.user = user
  end
end
